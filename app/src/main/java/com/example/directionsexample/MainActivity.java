package com.example.directionsexample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.Menu;
import android.widget.TextView;

import com.google.gson.Gson;

public class MainActivity extends Activity {
	
	TextView textview; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		textview = (TextView)findViewById(R.id.text1);
		new FetchTask().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public class FetchTask extends AsyncTask<Void, Void, String> {
		
		@Override
		protected void onPostExecute(String result) {
			Spanned html = Html.fromHtml(result);
			textview.setText(html);
		}

		protected String fetch() throws Exception {
			URL url = new URL("http://maps.googleapis.com/maps/api/directions/json?origin=700%20hidden%20ridge,%20irving,%20tx&destination=iowa&sensor=false");
			InputStream stream = (InputStream) url.getContent();
			String json = stream2string(stream);
			Gson gson = new Gson();
			ApiResult result = gson.fromJson(json, ApiResult.class);
			String out = "";
			for(Step step : result.routes[0].legs[0].steps) {
				out += step.html_instructions;
			}
			return out; 
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				return fetch(); 
			} catch(Exception ex) {
				ex.printStackTrace();
				return null; 
			}
		}
	}
	
	public String stream2string(InputStream in) throws IOException {
		InputStreamReader is = new InputStreamReader(in);
		StringBuilder sb=new StringBuilder();
		BufferedReader br = new BufferedReader(is);
		String read = br.readLine();

		while(read != null) {
		    //System.out.println(read);
		    sb.append(read);
		    read =br.readLine();

		}

		return sb.toString();
	}

}
